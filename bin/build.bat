REM @echo off
REM
REM Copyright (c) 2013 Belcarra Technologies 2005
REM
REM build.bat file.proj WIN TYPE ARCH
REM 
REM WIN = Vista | Win7 | Win8
REM TYPE = Release | Debug
REM ARCH = x86 | x64
REM
REM
REM C.f. msdn Building a Driver
REM
REM


REM if "%2" == "" goto :usage
REM if "%2" == "Vista" goto :arg1ok
REM if "%2" == "Win7" goto :arg1ok
REM if "%2" == "Win8" goto :arg1ok
REM if "%2" == "Win10" goto :arg1ok
REM goto :usage 

:arg1ok
if "%3" == "" goto :usage
if "%3" == "Release" goto :arg2ok
if "%3" == "Debug" goto :arg2ok
goto :usage 

:arg2ok
REM if "%4" == "" goto :usage
REM if "%4" == "x86" goto :arg3ok
REM if "%4" == "x64" goto :arg3ok
REM if "%4" == "arm" goto :arg3ok
REM if "%4" == "arm64" goto :arg3ok
REM if "%4" == "ARM" goto :arg3ok
REM if "%4" == "ARM64" goto :arg3ok
REM goto :usage 

:arg3ok

call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" %4

SET PATH=%PATH%;C:\Windows\Microsoft.NET\Framework\v4.0.30319

set 

rem MSBuild.exe /dfl /noconlog /fl /nr:false /nologo /m /ds /p:configuration="%2 %3" /p:target="%4"
rem MSBuild.exe usblan.vcxproj  /t:clean /t:build /nr:true /nologo /m:12 /ds /p:configuration="%2 %3" /p:target="%4"
rem MSBuild.exe  /t:build /nr:true /nologo /m:12 /ds /p:configuration="%2 %3" /p:target="%4"

@echo on
MSBuild.exe  %1 /t:clean /t:build /nr:true /nologo /m:12 /ds /p:configuration="%2 %3" /p:platform="%4"


if errlevel == 0 goto :eof

exit 1


@echo off
goto :eof

:usage

echo Error in script usage. The correct usage is:
echo    %0 Vista^|Win7^|Win8 Release^|Debug x86^|x64^|arm^|arm64
echo
echo
echo For example:
echo     %0 Win7 Release x64
echo
goto :eof

:eof

exit 0

