"
"VIMINIT='set autowrite redraw wrapmargin=54 tabstop=8 shiftwidth=8 smd nobk nowb cindent cino=:0p0t0 ruler';

" this seems necessary to make vim 5.1 do color
if has("terminfo")
    set t_Co=8
    set t_Sf=[3%p1%dm
    set t_Sb=[4%p1%dm
else
    set t_Co=8
    set t_Sf=[3%dm
    set t_Sb=[4%dm
endif                                                    

syntax on

" This is sl colour scheme
"
" canonical colors are:
"
"	black		normal
"	red		statements
"	green		special, comments
"	yellow		not used
"	blue		identifiers
"	magenta		pre processing directives
"	cyan		constants
"	white		not used

"						really		bright	easy	OK	hard	bad
"	Black                                                                   *
"	DarkBlue				light blue		        *
"	DarkGreen				light green		  		*
"	DarkCyan				light cyann				*
"	DarkRed					red			*
"	DarkMagenta				magenta				*
"	Brown					yellow						*
"	LightGray, LightGrey, Gray, Grey	white						*

"	DarkGray, DarkGrey			black		*	*
"	Blue, LightBlue				blue		*	*
"	Green, LightGreen			green		*			*
"	Cyan, LightCyan				cyan		*			*
"	Red, LightRed				red		*	*
"	Magenta, LightMagenta			magenta		*	*
"	Yellow					yellow		*				*
"	White					white		*

highlight Pmenu ctermfg=black ctermbg=lightgrey
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

if &background == "dark"
    hi Comment          term=bold       ctermfg=Cyan            guifg=#80a0ff
    hi Constant         term=underline  ctermfg=Magenta         guifg=#ffa0a0
    hi Special          term=bold       ctermfg=LightRed        guifg=Orange
    hi Identifier       term=underline  ctermfg=Cyan            guifg=#40ffff   cterm=bold
    hi Statement        term=bold       ctermfg=Yellow          guifg=#ffff60   gui=bold
    hi PreProc          term=underline  ctermfg=LightBlue       guifg=#ff80ff
    hi Type             term=underline  ctermfg=LightGreen      guifg=#60ff60   gui=bold
else

    if (v:version >= 700) 

    hi SpellBad         ctermfg=LightRed    term=Reverse        guisp=LightRed      gui=undercurl   ctermbg=White 
    hi SpellCap         ctermfg=Green       term=Reverse        guisp=Green         gui=undercurl   ctermbg=White 
    hi SpellLocal       ctermfg=Cyan        term=Underline      guisp=Cyan          gui=undercurl   ctermbg=White 
    hi SpellRare        ctermfg=Magenta     term=underline      guisp=Magenta       gui=undercurl   ctermbg=White 

    hi CursorLine       ctermbg=236         ctermfg=NONE        guibg=#2d2d2d       guifg=NONE      term=NONE           gui=NONE
    hi CursorColumn     ctermbg=236         ctermfg=NONE        guibg=#2d2d2d       guifg=NONE      term=NONE           gui=NONE
    hi MatchParen       ctermbg=Cyan        ctermfg=157         guibg=#2f2f2f       guifg=#d0ffc0   term=bold           gui=bold
    hi Pmenu            ctermbg=238         ctermfg=255         guibg=#444444       guifg=#ffffff   term=NONE           gui=NONE
    hi PmenuSel         ctermbg=148         ctermfg=0           guibg=#b1d631       guifg=#000000   term=NONE           gui=NONE

    endif " version 7+ 

    " //-- Stuff --//
    hi Constant         ctermfg=DarkGreen   	  	                            guifg=Magenta   term=underline  
    hi DiffDelete       ctermbg=DarkRed     ctermfg=White       guibg=DarkRed       guifg=White                         gui=NONE
    hi DiffAdd          ctermbg=DarkGreen   ctermfg=White       guibg=DarkGreen     guifg=White                         gui=NONE
    hi DiffText         ctermbg=LightCyan   ctermfg=Yellow      guibg=Lightblue     guifg=Yellow                        gui=NONE
    "hi DiffChange       ctermbg=LightBlue   ctermfg=White       guibg=LightBlue3    guifg=White                         gui=NONE
    hi DiffChange       ctermbg=LightCyan   ctermfg=White       guibg=LightBlue3    guifg=White                         gui=NONE

    " //-- Messages --//

    " //-- Syntax group --//
    hi Comment          ctermfg=DarkGrey       	                                    guifg=Blue      term=bold                  
    hi Special          ctermfg=DarkBlue	                                    guifg=SlateBlue term=bold       
    hi Identifier       ctermfg=DarkBlue                                            guifg=DarkCyan  term=underline  
    hi Statement        ctermfg=DarkRed        	                                    guifg=Brown     term=bold       gui=bold
    hi PreProc          ctermfg=DarkMagenta                                         guifg=Purple    term=underline  
    hi Type             ctermfg=DarkBlue                                            guifg=SeaGreen  term=underline  gui=bold

    " //-- Fold --//



    "   " //-- Stuff --//
    "   hi Cursor       ctermbg=Red         ctermfg=NONE        guibg=#8b3a3a       guifg=#e3e3e3                       gui=NONE
    "   hi Normal       ctermbg=NONE        ctermfg=LightGrey   guibg=#f7f5f3       guifg=#3d382e                       gui=NONE
    "   hi NonText      ctermbg=NONE        ctermfg=Black       guibg=#f7f5f3       guifg=#000030                       gui=NONE
    "   hi StatusLine   ctermbg=DarkGrey    ctermfg=Red         guibg=DarkGrey      guifg=LightRed                      gui=italic
    "   hi StatusLineNC ctermbg=Darkgrey    ctermfg=black       guibg=DarkGrey      guifg=Black                         gui=NONE
    "   hi VertSplit    ctermbg=NONE        ctermfg=NONE        guibg=NONE          guifg=Grey                          gui=NONE
    "   hi Visual       ctermbg=DarkRed     ctermfg=Red         guibg=#6b6b6b       guifg=#431818  term=reverse         
    "   hi Search       ctermbg=NONE        ctermfg=LightGrey   guibg=#80363a       guifg=#a69c89                       gui=NONE
    "   hi Label        ctermbg=NONE        ctermfg=NONE        guibg=NONE          guifg=#ffc0c0                       gui=NONE
    "   hi LineNr       ctermbg=NONE        ctermfg=Red         guibg=NONE          guifg=#A39274                       gui=NONE

    "   " //-- Messages --//
    "   hi MoreMsg      ctermbg=NONE        ctermfg=DarkGreen   guibg=NONE          guifg=SeaGreen term=bold,italic     gui=bold
    "   hi question     ctermbg=NONE        ctermfg=DarkGreen   guibg=NONE          guifg=SeaGreen term=standout        gui=bold

    "   " //-- Syntax group --//
    "   hi Todo         ctermbg=NONE        ctermfg=NONE        guibg=NONE          guifg=LightBlue                    gui=bold,italic
    "   hi Number       ctermbg=NONE        ctermfg=NONE        guibg=NONE          guifg=#001a33                      gui=NONE
    "   "
    "   " //-- Fold --//
    "   hi Folded       ctermbg=NONE        ctermfg=NONE        guibg=#a69c89       guifg=#001a33                      gui=italic
    "   hi FoldColumn   ctermbg=NONE        ctermfg=Yellow      guibg=#6699CC       guifg=#0000EE                      gui=NONE


endif                                                                        
":colorscheme faded-white

" remote SCCS files
":argdelete SCCS/*

set autoindent
set shiftwidth=8
set tabstop=8
set wrapmargin=4
set expandtab

set showmode			" display insert, replace or visual message
set nobackup			" don't keep backup file
set nowritebackup		" don't create backup file
set autowrite			" always write the file before next, rewind etc
set ruler 			" show us where we are in file
set cpo=ceFS 			" allow <esc> notation
"set hls 			" turn on highlighting of search targets
set incsearch			" show next match for search as pattern is entered

set nocompatible        	" Use Vim defaults (much better!)           

" We don't want screen restored on exit
set t_ti= t_te=   
set restorescreen=off
"au VimLeave * :!clear 

"set t_ti=7r?47h
"set t_te=[?47l8

" add <> to mps
set mps =(:),[:],{:},<:>


" verbosity
" set verbose=9

"au BufNewFile,BufRead *.s4,*.sgm,*.sgml,*.sect                   so $VIM/syntax/sgml.vim



":map <f2> :map<c-m>
":map <f3> :inoremap<c-m>

:map <f1> zc
":map <f1> :set laststatus=2<c-m>:set statusline=%{GitBranch()}<c-m>
":map <f2> :set paste<c-m>
":map <f3> :set nopaste<c-m>
:map <f2> :set paste!<c-m>:set paste?<c-m>
":map <f3> :set nopaste<c-m>
:map <f3> :set laststatus=2<c-m>:set statusline=%{GitBranch()}<c-m>

:map <f4> :set number!<c-m>:set number?<c-m>

:map <f5> :set list!<c-m>:set list?<c-m>
:map <f6> :set hls!<c-m>:set hls?<c-m>
:map <f7> :set ignorecase!<c-m>:set ignorecase?<c-m>
:map <f8> :set autoindent!<c-m>:set autoindent?<c-m>

:map <f9>  :set expandtab!<c-m>:set expandtab?<c-m>
:map <f10> :if &tabstop == 4<c-m>set tabstop=8<c-m>else<c-m>set tabstop=4<c-m>endif<c-m>:set tabstop<c-m>
:map <f11> :if &shiftwidth == 4<c-m>set shiftwidth=8<c-m>else<c-m>set shiftwidth=4<c-m>endif<c-m>:set shiftwidth<c-m>
":map <f12> :if &wrapmargin == 4<c-m>set wrapmargin=64<c-m> elseif &wrapmargin == 64<c-m>set wrapmargin=34<c-m> else<c-m>set wrapmargin=4<c-m>endif<c-m>:set wrapmargin<c-m>
set textwidth=100

":set wrapmargin=(&columns-80)



iab Ydate <c-r>=strftime("%a %b %d %T %Z %Y")<cr>
iab Yme Stuart Lynne
iab Ytag <c-v>{<c-v>{<c-v>{<cr><cr><c-v>}<c-v>}<c-v>}
iab Yctag /* <c-v>{<c-v>{<c-v>{<cr><cr><c-v>}<c-v>}<c-v>} */
iab Ycctag /* <c-v>{<c-v>{<c-v>{ */ <cr><cr>/* <c-v>}<c-v>}<c-v>} */
au BufNewFile *.hpp      if filereadable($HOME . "/.vim/skel.hpp")  | r $HOME/.vim/skel.hpp  | 1,1d | endif
au BufNewFile *.h        if filereadable($HOME . "/.vim/skel.h")    | r $HOME/.vim/skel.h    | 1,1d | endif
au BufNewFile *.m        if filereadable($HOME . "/.vim/skel.c")    | r $HOME/.vim/skel.c    | 1,1d | endif
au BufNewFile *.c        if filereadable($HOME . "/.vim/skel.c")    | r $HOME/.vim/skel.c    | 1,1d | endif
au BufNewFile *.cc       if filereadable($HOME . "/.vim/skel.cc")   | r $HOME/.vim/skel.cc   | 1,1d | endif
au BufNewFile *.cpp      if filereadable($HOME . "/.vim/skel.cc")   | r $HOME/.vim/skel.cc   | 1,1d | endif
au BufNewFile *.pov      if filereadable($HOME . "/.vim/skel.pov")  | r $HOME/.vim/skel.pov  | 1,1d | endif
au BufNewFile *.html     if filereadable($HOME . "/.vim/skel.html") | r $HOME/.vim/skel.html | 1,1d | endif
au BufNewFile *.pl       if filereadable($HOME . "/.vim/skel.pl")   | r $HOME/.vim/skel.pl   | 1,1d | endif
au BufNewFile *.sgm      if filereadable($HOME . "/.vim/skel.sgm") | r $HOME/.vim/skel.html | 1,1d | endif
au BufNewFile *.s4       if filereadable($HOME . "/.vim/skel.sgm") | r $HOME/.vim/skel.html | 1,1d | endif

"augroup XXX
"au BufReadPost * if b:current_syntax == "sgml"
"  " Remove all cprog autocommands
"  au!
"


"  " When starting to edit a file:
"  "   For *.c and *.h files set formatting of comments and set C-indenting on.
"  "   For other files switch it off.
"  "   Don't change the order, it's important that the line with * comes first.
"  autocmd BufRead *       set formatoptions=tcql nocindent comments&
"  autocmd BufNewFile,BufRead *.s4,*.sgm,*.sgml,*.sect so $HOME/.vimrc-sgml
"au BufReadPost * endif

":set verbose=2000


au BufNewFile,BufRead * if exists("b:current_syntax")
"au BufNewFile,BufRead * echo b:current_syntax

au BufNewFile,BufRead * if b:current_syntax == "sgml"
    au BufNewFile,BufRead * set formatoptions=tcql nocindent comments&
    au BufNewFile,BufReadPost * so $HOME/.vimrc-sgml
au BufNewFile,BufRead * endif

au BufNewFile,BufRead * if b:current_syntax == "make"
    au BufNewFile,BufRead * set formatoptions=tcql nocindent comments&
    au BufNewFile,BufReadPost * so $HOME/.vimrc-make
au BufNewFile,BufRead * endif


au BufNewFile,BufRead * if b:current_syntax == "sh"
    au BufNewFile,BufRead * set formatoptions=tcql nocindent comments&
    au BufNewFile,BufReadPost * so $HOME/.vimrc-sh
au BufNewFile,BufRead * endif

au BufNewFile,BufRead * if b:current_syntax == "perl"
    au BufNewFile,BufRead * set formatoptions=tcql nocindent comments&
    au BufNewFile,BufReadPost * so $HOME/.vimrc-perl
au BufNewFile,BufRead * endif

au BufNewFile,BufRead * if b:current_syntax == "objcpp"
    au BufNewFile,BufRead * set formatoptions=tcql nocindent comments&
    au BufNewFile,BufReadPost * so $HOME/.vimrc-cprog
    au BufNewFile,BufReadPost * if filereadable(".vimrc-clocal")| so .vimrc-clocal| endif
au BufNewFile,BufRead * endif

au BufNewFile,BufRead * if b:current_syntax == "objc"
    au BufNewFile,BufRead * set formatoptions=tcql nocindent comments&
    au BufNewFile,BufReadPost * so $HOME/.vimrc-cprog
    au BufNewFile,BufReadPost * if filereadable(".vimrc-clocal")| so .vimrc-clocal| endif
    " map <C-L> :!ctags -R --objc-kinds=+PiIMCZ --fields=iaS --extra=+q
au BufNewFile,BufRead * endif

au BufNewFile,BufRead * if b:current_syntax == "cpp"
    au BufNewFile,BufRead * set formatoptions=tcql nocindent comments&
    au BufNewFile,BufReadPost * so $HOME/.vimrc-cprog
    au BufNewFile,BufReadPost * if filereadable(".vimrc-clocal")| so .vimrc-clocal| endif
au BufNewFile,BufRead * endif

au BufNewFile,BufRead * if b:current_syntax == "c"
    au BufNewFile,BufRead * set formatoptions=tcql nocindent comments&
    au BufNewFile,BufReadPost * so $HOME/.vimrc-cprog
    au BufNewFile,BufReadPost * if filereadable(".vimrc-clocal")| so .vimrc-clocal| endif
    " map <C-L> :!ctags -R --c-kinds=+p --fields=iaS --extra=+q
au BufNewFile,BufRead * endif

au BufNewFile,BufRead * if b:current_syntax == "c++"
    au BufNewFile,BufRead * set formatoptions=tcql nocindent comments&
    au BufNewFile,BufReadPost * so $HOME/.vimrc-cprog
    au BufNewFile,BufReadPost * if filereadable(".vimrc-clocal")| so .vimrc-clocal| endif
au BufNewFile,BufRead * endif

au BufNewFile,BufRead * endif

"augroup mkd
"  autocmd BufRead *.mkd  set ai formatoptions=tcroqn2 comments=n:>
"augroup END

if !exists("did_asm_syntax_inits")
  let did_asm_syntax_inits = 1

  " The default methods for highlighting.  Can be overridden later
  hi link asmSection    Special
  hi link asmLabel      Label
  hi link asmComment    Comment
  hi link asmDirective  Statement

  hi link asmInclude    Include
  hi link asmCond       PreCondit
  hi link asmMacro      Macro

  hi link hexNumber     Number
  hi link decNumber     Number
  hi link octNumber     Number
  hi link binNumber     Number

" My default color overrides:
hi asmSpecialComment ctermfg=red
hi asmIdentifier ctermfg=darkblue
hi asmType ctermbg=black ctermfg=brown

endif


" Search for selected text, forwards or backwards.
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>

autocmd BufEnter * let &titlestring = $USER. "@" . hostname() . ":" . expand("%:p") 

set spell
setlocal spell spelllang=en_us
set spellfile=~sl/.vim/spellfile.add

autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType c set omnifunc=ccomplete#Complete

set foldmethod=marker
" Set a nicer foldtext function
set foldtext=MyFoldText()
function! MyFoldText()
  let line = getline(v:foldstart)
  if match( line, '^[ \t]*\(\/\*\|\/\/\)[*/\\]*[ \t]*$' ) == 0
    let initial = substitute( line, '^\([ \t]\)*\(\/\*\|\/\/\)\(.*\)', '\1\2', '' )
    let linenum = v:foldstart + 1
    while linenum < v:foldend
      let line = getline( linenum )
      let comment_content = substitute( line, '^\([ \t\/\*]*\)\(.*\)$', '\2', 'g' )
      if comment_content != ''
        break
      endif
      let linenum = linenum + 1
    endwhile
    let sub = initial . ' ' . comment_content
  else
    let sub = line
    let startbrace = substitute( line, '^.*{[ \t]*$', '{', 'g')
    if startbrace == '{'
      let line = getline(v:foldend)
      let endbrace = substitute( line, '^[ \t]*}\(.*\)$', '}', 'g')
      if endbrace == '}'
        let sub = sub.substitute( line, '^[ \t]*}\(.*\)$', '...}\1', 'g')
      endif
    endif
  endif
  let n = v:foldend - v:foldstart + 1
  let info = " " . n . " lines"
  let sub = sub . "                                                                                                                  "
  let num_w = getwinvar( 0, '&number' ) * getwinvar( 0, '&numberwidth' )
  let fold_w = getwinvar( 0, '&foldcolumn' )
  let sub = strpart( sub, 0, winwidth(0) - strlen( info ) - num_w - fold_w - 1 )
  return sub . info
endfunction

" set t_Co=16

highlight MyComments ctermfg=black ctermbg=10
autocmd Filetype txt MyComments /_.* .*_\|\*\*.*$/
autocmd Filetype cpp match MyComments /\*\*.*$/
autocmd Filetype c match MyComments /\*\*.*$/


"hi Search       ctermbg=14       ctermfg=LightGrey   guibg=#80363a       guifg=#a69c89                       gui=NONE
"hi Search       ctermbg=NONE        ctermfg=LightGrey   guibg=#80363a       guifg=#a69c89                       gui=NONE

"highlight Search term=reverse

highlight Search ctermfg=black ctermbg=11
highlight cTodo ctermfg=black ctermbg=11 
"highlight cTodo ctermfg=black ctermbg=14 " light blue 
"highlight cTodo ctermfg=black ctermbg=10 " light green
"highlight cTodo ctermfg=black ctermbg=9 " light red 
highlight cTodo ctermfg=black ctermbg=10 " light red 

"set foldmethod=syntax
"let c_no_comment_fold = 1
"let c_no_if0_fold = 1
"let c_no_block_fold = 1
"syntax region cBlock          start="{" end="}" transparent
"syntax region MyDoxComment      start="/\*\*" end="^ \*/" transparent fold

set nobackup
set noswapfile
"set mouse=a


"autocmd Filetype cpp :set fmr=/*,*/ fdm=marker fdc=1
"autocmd Filetype c :set fmr=/*,*/ fdm=marker fdc=1
"autocmd Filetype h :set fmr=/*,*/ fdm=marker fdc=1

