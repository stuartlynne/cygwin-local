#!/bin/bash

ARG0=`basename $0`

echo $ARG0

function getone() {
        # setup for no canonical processing, 1 char min etc
        stty -icanon -echo min 1 2>/dev/null > /dev/null

        # use dd to read 1 char
        dd bs=1 count=1 2>/dev/null
        stty $savetty 2>/dev/null >/dev/null
}

yesno(){
        echo -en "${bd}$1${Off}"
        yesno_reply=`getone` ; echo -en "$yesno_reply"
        case "$yesno_reply" in
        [Yy]|[Yy]es|[Oo]ui|[Ss]i|[jJ]a) return 0 ;;
        *) return 4 ;;
        esac
}

waitforany(){
        echo -en "\n"
        echo -en "${bd}Press any key to continue ${Off}"
        reply=`getone` ; echo -en "$reply"
        tput reset
}

yesnotest() {
        #echo -en "YesNo test: "
        yesno "YesNo Test: "
        if [ $? -eq 0 ] ; then echo YES ; else echo NO ; fi
}


function STDERR () {
        echo -en "$*\n" 1>&2
}

function ERREXIT() {
        STDERR "\n$*\n"
        exit 1
}

function ERRUSAGE() {
        STDERR
        STDERR "Usage: ${ARG0} command [ [remote] repository [branch] ]\n" 
        ERREXIT "\t$*"
}

function GIT() {
        #echo GIT- $*
        ( set -x; git "$@"; )
}

function GITFLOW(){
	git-flow $*
}

function ISNAME() {
        expr "$1" : ".*\(@\).*"  2> /dev/null > /dev/null
        return $?
}

function SSH() {
        [ $# -lt 2 ] && ERREXIT "Error: SSH requires two args"
        SSHREMOTE=$1 && shift
        ( set -x; ssh -A "${SSHREMOTE}" "pwd; ${@}" )
        #( set -x; ssh -A "${SSHREMOTE}" "cd; pwd; ${@}" )
        return $?
}

function OLDERRIFEXISTS() {
        STDERR "\Verify that ${BTC_SSH}/${BTC_DIR}/${1} does NOT exists"
        SSH $BTC_SSH "test -d ${BTC_DIR}/${1}" && ERREXIT "Error: ${BTC_SSH}:${BTC_DIR}/${1} already exists"
}

function OLDERRUNLESSEXISTS() {
        STDERR "\nVerify that ${BTC_SSH}/${BTC_DIR}/${1} does exist"
        SSH $BTC_SSH "test -d ${BTC_DIR}/${1}" || ERREXIT "Error: ${BTC_SSH}:${BTC_DIR}/${1} does not exist"
}

function ERRIFEXISTS() {
        STDERR "\Verify that ${1} does NOT exists"
        git ls-remote ${1} && ERREXIT "Error: ${1} already exists"
}

function ERRUNLESSEXISTS() {
        STDERR "\nVerify that ${1} does exist"
        GIT ls-remote ${1} || ERREXIT "Error: ${1} does not exist"
}

function ENV() {
        [ -n "${2}" ] && STDERR "$1: $2" || STDERR "$1: ** NOT SET **"
}


function UITEM() {
        ITEM=$1
        ARGS=$2
        shift 2
        printf "%14s: %-20s %s\n" "${ITEM}" "${ARGS}" "${*}" 1>&2
}


function USAGE() {
        STDERR "\n${ARG0}\n"
        STDERR
        STDERR "Facilities use of git flow repositories"
        STDERR
        STDERR ""
        STDERR
        STDERR

        STDERR "The commands available depend on where this script is run. Outside of a GIT"
        STDERR "repository commands are available to create a new empty git repository or"
        STDERR "to clone a repository"
        STDERR
        STDERR "Inside a GIT repository commands are available to sync the original parent"
        STDERR "GIT repository changes to the local repository or to publish the local"
        STDERR "changes to the remote repository."
        STDERR
        STDERR "All repositories are created and initialized to work with git-flow. This mandates"
        STDERR "a specific branching model: master<->develop<->feature."
        STDERR
        STDERR "Show help"
        STDERR "\tbtcgit help "
        STDERR

        case ${GIT} in
        n)
                GITUSAGE
                INGITUSAGE
                ;;
        y)
                INGITUSAGE
                GITUSAGE
                ;;
        esac

}

function GITUSAGE() {

        STDERR "Outside of a GIT directory"
        STDERR
        STDERR
        STDERR "Create an empty, bare, git-flow repository in current directory, this should be"
        STDERR "typically be done in your ~/git directory."
        STDERR "\tbtcgit empty repository_name"
        STDERR
        STDERR "Clone a remote repository in current directory, typically from user@localhost:~/git/reponame"
        STDERR "\tbtcgit clone repository_name"
        STDERR

}


function INGITUSAGE() {
        STDERR "Inside of a GIT directory"
        STDERR
        STDERR "Show branch and remote information about this repository"
        STDERR "\tbtcgit status"
        STDERR
        STDERR "Add a new remote origin and push to it"
        STDERR "\tbtcgit link_push"
        STDERR
        STDERR "Fetch, checkout and merge with origin/master and origin/develop"
        STDERR "\tbtcgit sync"
        STDERR
        STDERR "Push with --mirror to origin"
        STDERR "\tbtcgit publish"
        STDERR

}


# ############################################################################################### #

function bareold() {

        REPO=$1

        [ -z "$REPO" ] && ERREXIT "Error: "$REPO" is empty"

        if [ ! expr "$REPO" : ".*\(.git\)" ] ; then
                $REPO="$REPO.git"
        fi

        [ -d "$REPO" ] && ERREXIT "Error: $REPO already exists"

        STDERR
        STDERR "\nCreating --bare $REPO repository"
        GIT init --bare "${REPO}"

        STDERR "\nFinish\n"
}                

function bare() {

        REPO=$1

        [ -z "$REPO" ] && ERREXIT "Error: "$REPO" is empty"

        if [ ! expr "$REPO" : ".*\(.git\)" ] ; then
                $REPO="$REPO.git"
        fi

        [ -d "$REPO" ] && ERREXIT "Error: $REPO already exists"

        STDERR
        STDERR "\nCreating --bare $REPO repository"
        GIT init --bare "${REPO}"
        
        cd ${REPO}

        STDERR
        STDERR "\nInitializing git flow in $REPO repository"
        GITFLOW init < /dev/null

        STDERR
        STDERR "\nCreating and adding .gitignore"
        touch .gitignore
        GIT add . 
        GIT commit -m "GIT Repository ${REPO} initialized" -a

        STDERR
        STDERR "\nFinish\n"
}                

function empty() {

        REPO=$1

        [ -z "$REPO" ] && ERREXIT "Error: "$REPO" is empty"

set -x
        if [ ! expr "$REPO" : ".*\(.git\)" > /dev/null ] ; then
                $REPO="$REPO.git"
        fi

        [ -d "$REPO" ] && ERREXIT "Error: $REPO already exists"

        STDERR
        STDERR "\nCreating base $REPO repository"
        mkdir -p ${REPO}
        pushd ${REPO}
        GIT init

        STDERR
        STDERR "\nInitializing git flow in $REPO repository"
        GITFLOW init < /dev/null

        STDERR
        STDERR "\nCreating and adding .gitignore"
        touch .gitignore
        GIT add . 
        GIT commit -m "GIT Repository ${REPO} initialized" 

        popd

        GIT clone --bare ${REPO} ${REPO}.git

        mv ${REPO} ${REPO}-

        STDERR
        STDERR "\nFinish\n"
}                

function clone() {

        STDERR "Cloning...."

        REPO=$1
        shift

        [ -z "$REPO" ] && ERREXIT "Error: "$REPO" is empty"

        expr "$REPO" : ".*\(\.git\)" || REPO="$REPO.git"
        
        DIR=$(echo $(basename ${REPO}) | sed 's/\.git$//')

        STDERR REPO: $REPO
        STDERR DIR: $DIR

        ERRUNLESSEXISTS $REPO
        
        [ -d "$DIR" ] && ERREXIT "Error: $DIR already exists"

       
        STDERR
        git clone "${REPO}" 

        [ -d "$DIR" ] || ERREXIT "Error: git clone $REPO failed"
        
        cd $DIR

        STDERR "\nCheckout master branch\n"
        GIT checkout --track -b master origin/master

        STDERR "\nInitialize git-flow\n"
        GIT flow init < /dev/null > /dev/null

        STDERR "\nCheckout develop branch\n"
        GIT checkout develop


        STDERR "\nFecth all\n"
        GIT fetch -v --all
        GIT fetch origin "refs/notes/*:refs/notes/*"
        
        STDERR "\nSynchronize and merge master branch from origin/master\n"
        GIT checkout master
        GIT merge origin/master
        
        STDERR "\nSynchronize and merge develop branch from origin/develop\n"
        GIT checkout develop
        GIT merge origin/develop
        STDERR "\n"

        STDERR
        STDERR "\nFinish\n"
}                

function link_push() {
        STDERR "Link and Push ....."


        REPO=$1
        shift

        [ -z "$REPO" ] && ERREXIT "Error: "$REPO" is empty"

        expr "$REPO" : ".*\(\.git\)" || REPO="$REPO.git"

        STDERR REPO: $REPO

        if [ $# -eq 1 ] ; then
                
                BRANCH=$1
                shift
                STDERR BRANCH: $BRANCH
                BRANCHOPT="-t $BRANCH"
        else
                unset BRANCHOPT
        fi
        

        ERRIFEXISTS $REPO
       
        STDERR "Setting remote for github"
        GIT remote add origin ${REPO}

        STDERR "Pushing"
        GIT push -v --mirror
}


# ############################################################################################### #


# ############################################################################################### #

#[ -z "${BTC_SSH}" ] && ERREXIT "Usage: please set BTC_SSH to your BTC home direcotory ssh URL" 
#echo BTC_SSH: $BTC_SSH
#[ $# -eq 0 ] && ERRUSAGE 

#set -x
COMMAND=$1
shift

git status > /dev/null 2>&1

if [ $? -eq 0 ] ; then

        echo 
        echo ${ARG0}: in GIT repository
        echo 
        export GIT='y'

else
        echo 
        echo ${ARG0}: not in GIT repository
        echo 
        export GIT='n'
fi


# process args
# Generically speaking
#
#       command [remote] repository [arg] .....
#


#ISNAME $1 && REMOTE=$1 && shift || REMOTE=$BTC_SSH

#USER=$(expr "${REMOTE}"  : "\(.*\)@.*")

# ############################################################################################### #

#if [ -z "$BTC_PATH" ] ; then

if [ ${GIT} = 'n' ] ; then

        #
        # WE ARE NOT INSIDE a git repository
        #
        
        case $COMMAND in

        bare)
                [ $# -lt 1 ] && ERRUSAGE "$COMMAND requires a local repository name argument"
                REPO=$1
                shift
                bare "${REPO}"
                ;;

        empty)
                [ $# -lt 1 ] && ERRUSAGE "$COMMAND requires a local repository name argument"
                REPO=$1
                shift
                empty "${REPO}"
                ;;

        clone)
                clone $1 
                ;;

        help)
                USAGE
                ;;
        *)
                USAGE
                ;;
        esac

# ############################################################################################### #
else
        #
        # If we ARE INSIDE a git repository
        #
        case $COMMAND in

                # test
        help)
                USAGE
                ;;

        status)
                GIT branch -r -v
                GIT remote -v
                GIT status
                ;;

        link_push)
                link_push $1
                ;;
        sync)
                GIT fetch -v --all
                GIT fetch origin "refs/notes/*:refs/notes/*"
                GIT checkout master
                GIT merge origin/master
                GIT checkout develop
                GIT merge origin/develop
                ;;

        pull)
                echo COMMAND: $COMMAND
                echo ARGS: $*
                ;;

        publish)
                GIT push -v --mirror
                #GIT push -v --all --tags
                #GIT push origin "refs/notes/*:refs/notes/*"
                ;;


        *)
                ERRUSAGE Command \"$COMMAND\" Unknown
                ;;
        esac

        # ############################################################################################### #



fi
exit 0
