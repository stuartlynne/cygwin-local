# .bashrc

#echo "~sl/.bashrc"
#set -x

# User specific aliases and functions

set completion-ignore-case on

#export PATH=$PATH:$HOME/bin
if echo $PATH | grep -qv "$HOME/bin"; then
    #echo fiddling path....
    PATH=$HOME/bin:$PATH
fi

#PATH=$PATH:~/.wine/drive_c/windows/system32

# Source global definitions
#if [ -f /etc/bashrc ]; then
#	. /etc/bashrc
#fi

# local
[ -f /etc/bash_completion ] && . /etc/bash_completion

#if [ -f ~/.ssh-bashrc ]; then
#	. ~/.ssh-bashrc
#fi

[ -f ~/.aliases ] && . ~/.aliases


#trap echo SIGINT;
export X=export
export TZ=PST8PDT
export HISTCONTROL=ignoredups
export command_oriented_history= 
export ignoreeof=2;
#export CDPATH=./:../:/:/usr:/var:/opt:/usr/local:/usr/spool:/etc:/usr/local/lib:/lib/:/usr/lib;
export CDPATH=./:../
export H=`hostname`
export PAGER=less
export VIEWER=less
export NOMETAMAIL=
export ORGANIZATION=none
export EDITOR=vi
export HISTFILE=
export PAGESTOP=^------- ;
export EXINIT='set autoindent autowrite redraw wrapmargin=4 tabstop=8 shiftwidth=4 smd nobk nowb';
export TERM=rxvt-256color
export TRNINIT="-e"
export BK_LICENSE=ACCEPTED
export BK_USER=sl
export BK_HOST=belcarra.com
export LESS="-X"

export PROMPT_DIRTRIME=4

#export U=`expr "\`id\`" : ".*=[0-9]*(*\(.*\)) g"`; 
export U=$(whoami)
export H=$HOST; 


case $U in \
             root)C1=1;C2=2;P='#';;\
             sl)C1=4;C2=1;P='%';;\
             *)C1=5;C2=1;P='$';;\
        esac; \

export PSC1ON="\[\033[3${C1}m\]"
export PSC2ON="\[\033[3${C2}m\]"
export PSCOFF="\[\033[0m\]"

function do_ps1() {

        if [ "$OSTYPE" = "cygwin" ] ; then
                if [ "${PWD}" != "${SAVED_PWD}" ] ; then
                        SAVED_PWD=$PWD
                        export GIT_BRANCH="$(__git_ps1)";
                        [ -n "${GIT_BRANCH}" ] && export PSG="$GIT_BRANCH" || unset PSG;
                fi
        else
                export GIT_BRANCH="$(__git_ps1)";
                [ -n "${GIT_BRANCH}" ] && export PSG="$GIT_BRANCH" || unset PSG;
        fi

        [ -n "${DISPLAY}" ] && echo -en "\033]0;$U@$H:$PWD\007" 

        export PSA="\d \\t"
        export PSB="\u@$H:\w"
        export PSC="[\!:\#] $P "

        export PS1="${PSC1ON}${PSA}${PSC2ON}${PSG}${PSC1ON}\n${PSB}\n${PSC}${PSCOFF}"
}


unset PROMPT_COMMAND

. /usr/local/bin/git-sh-prompt

export PROMPT_COMMAND='do_ps1'

mount -a

PATH="/home/sl/perl5/bin${PATH+:}${PATH}"; export PATH;
PERL5LIB="/home/sl/perl5/lib/perl5${PERL5LIB+:}${PERL5LIB}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/sl/perl5${PERL_LOCAL_LIB_ROOT+:}${PERL_LOCAL_LIB_ROOT}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/sl/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/sl/perl5"; export PERL_MM_OPT;


SSHAGENT=/usr/bin/ssh-agent
SSHAGENTARGS="-s"
if [ -z "$SSH_AUTH_SOCK" -a -x "$SSHAGENT" ]; then
	eval `$SSHAGENT $SSHAGENTARGS`
	trap "kill $SSH_AGENT_PID" 0
	ssh-add ~/.ssh/id_rsa
fi

